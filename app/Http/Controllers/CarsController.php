<?php

namespace App\Http\Controllers;

use App\Entities\Car;
use App\Http\Requests\CarRequest;
use App\Repositories\CarRepository;
use Illuminate\Http\Request;

class CarsController extends Controller
{
    /**
     * @var CarRepository
     */
    private $cars;

    /**
     * CarsController constructor.
     * @param CarRepository $cars
     */
    public function __construct(CarRepository $cars)
    {
        $this->cars = $cars;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $cars = $this->cars->getAll();

        return view('cars.index', ['title' => 'Cars List', 'cars' => $cars->toArray()]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('cars.create', ['title' => 'Create Car', 'car' => (new Car([]))->toArray()]);
    }

    /**
     * @param $id
     * @param string $view
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id, $view = 'show')
    {
        $car = $this->cars->getById($id);

        abort_if(! $car, 404);

        return view("cars.$view", ['title' => $car->getModel(), 'car' => $car->toArray()]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        return $this->show($id, 'edit');
    }

    /**
     * @param $id
     * @param CarRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update($id, CarRequest $request)
    {
        $car = $this->cars->getById($id);

        abort_if(! $car, 404);

        $updatedCar = new Car(array_merge($car->toArray(), $request->except('id')));
        $updatedCar->setId($id);

        $this->cars->update($updatedCar);

        return $this->show($id);
    }

    /**
     * @param CarRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function store(CarRequest $request)
    {
        $car = new Car($request->all());
        $this->cars->addItem($car);

        return $this->index();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function destroy(Request $request)
    {
        abort_if(! $this->cars->getById($request->get('id')), 404);

        $this->cars->delete($request->get('id'));

        return $this->index();
    }
}
