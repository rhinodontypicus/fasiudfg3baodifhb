@extends('layouts.master')

@section('content')
    <h2>{{ $car['model'] }}</h2>

    <form action="{{ route('cars.store') }}" method="POST">
        @include('cars._form', compact('car'))
    </form>
@endsection