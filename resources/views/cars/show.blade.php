@extends('layouts.master')

@section('content')
    <h2>{{ $car['model'] }}</h2>
    <p><strong>Year</strong>: {{ $car['year'] }}</p>
    <p><strong>Registration number</strong>: {{ $car['registration_number'] }}</p>
    <p><strong>Color</strong>: {{ $car['color'] }}</p>
    <p><strong>Price</strong>: ${{ $car['price'] }}</p>

    <a href="{{ route('cars.edit', $car['id']) }}" class="btn btn-default edit-button" style="display: inline-block">Edit</a>
    <form action="{{ route('cars.destroy') }}" method="POST" style="display: inline-block">
        <input type="hidden" name="id" value="{{ $car['id'] }}">
        {{ method_field('DELETE') }}
        {{ csrf_field() }}
        <button type="submit" class="btn btn-danger delete-button">Delete</button>
    </form>
@endsection