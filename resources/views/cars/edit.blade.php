@extends('layouts.master')

@section('content')
    <h2>{{ $car['model'] }}</h2>

    <form action="{{ route('cars.update', $car['id']) }}" method="POST">
        {{ method_field('PUT') }}

        @include('cars._form', compact('car'))
    </form>
@endsection