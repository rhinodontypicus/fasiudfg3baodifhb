<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('cars')->name('cars.')->group(function () {
    Route::get('/', 'CarsController@index')->name('index');
    Route::get('/create', 'CarsController@create')->name('create');
    Route::get('{id}', 'CarsController@show')->name('show');
    Route::get('{id}/edit', 'CarsController@edit')->name('edit');
    Route::put('{id}', 'CarsController@update')->name('update');
    Route::post('/', 'CarsController@store')->name('store');
    Route::delete('/', 'CarsController@destroy')->name('destroy');
});